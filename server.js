// Dependencies
// -----------------------------------------------------
var express = require('express');
var app = express();

var mongoose = require('mongoose');
var path = require('path');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');


// Express Configuration
// -----------------------------------------------------
// Sets the connection to MongoDBpath
mongoose.connect('mongodb://localhost:27017/contacts', function(err, res) {
 if(err) throw err;
 console.log('Connected to Database');
});

// Logging and Parsing
app.use(express.static(path.join(__dirname, './'))); 	 //sets the static files location to public
app.use(morgan('dev')); 								//log with Morgan
app.use(bodyParser.json()); 						   //parse application/json
app.use(bodyParser.urlencoded({extended: false})); 	  //parse application/x-www-form-urlencoded
app.use(methodOverride());

// Routes
// ------------------------------------------------------
require('./app/routes/routes.js')(express,app);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
 var err = new Error('Not Found');
 err.status = 404;
 next(err);
});

// Listen
// -------------------------------------------------------
app.listen(3000, function() { 
 console.log('App listening on port 3000');
});


// ContactsApp
// -------------------------------------------------------
// "author":"Alex Cuesta <xelak88@gmail.com>
// "description": "Contact List MEAN-stack"