var contactsApp = angular.module('contactsApp', ['ngMaterial','ngRoute']);

contactsApp.config(['$mdIconProvider','$mdThemingProvider','$routeProvider',
	function($mdIconProvider,$mdThemingProvider,$routeProvider) {

	$mdIconProvider
		.icon("menu","img/icons/menu.svg", 24)
		.icon("avatar","img/icons/person.svg", 24)
		.icon("add","img/icons/person_add.svg", 24)
        .icon("phone","img/icons/phone.svg", 24)
        .icon("email","img/icons/email.svg", 24)
        .icon("address","img/icons/address.svg", 24)
        .icon("birthday","img/icons/birthday.svg", 24)
        .icon("more","img/icons/more.svg", 24)
        .icon("less","img/icons/less.svg", 24)
		.icon("search","img/icons/search.svg", 18)
		.icon("delete","img/icons/delete.svg", 18);

	$mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('red');

    $routeProvider.
    when('/view',
    {
    	templateUrl: 'partials/contactInfo.html'
    })
    .when('/form',
    {
    	templateUrl: 'partials/addContactForm.html'
    })
    .otherwise({ redirectTo: '/view'});


    // "author":"Alex Cuesta <xelak88@gmail.com>

}]);