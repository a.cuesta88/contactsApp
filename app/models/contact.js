var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactSchema = new Schema({ 
 name: 		 { type: String, required: true },
 surname: 	 { type: String, required: true },
 phone: 	 { type: Number },
 email: 	 { type: String },
 address:    {
 	country: { type: String },
 	city:    { type: String },
 	street:  { type: String },
 	pc: 	 { type: Number }
 },
 birthday: 	 { type: Date },
 created_at: {type: Date, default: Date.now},
 updated_at: {type: Date, default: Date.now}
});

// Sets the created_at parameter equal to the current time
contactSchema.pre('save', function(next){
 now = new Date();
 this.updated_at = now;
 if(!this.created_at) {
 	this.created_at = now
 }
 next();
});

module.exports = mongoose.model('Contact', contactSchema);