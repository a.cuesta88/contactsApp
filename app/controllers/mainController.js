contactsApp.controller("mainController",['$scope','$location','$http','$mdSidenav','$mdDialog','$mdToast',
	function($scope,$location,$http,$mdSidenav,$mdDialog,$mdToast) {

	var self 		  = this;
	self.save 		  = createOrUpdateContact;
	self.delete 	  = deleteContact;
	self.toggleInfo   = toggleMoreInfo;
  	self.toggleList   = toggleContactList;
  	self.view 		  = select;
  	self.goto 		  = go_to;

  	$scope.expand ="more";

  	cleanForm();
	getContacts();

	  // ********************************* //
  	 // 			REST				  //
  	// ********************************* //

	// Create or Update Contact
	function createOrUpdateContact(){

		if($scope.formData._id == null){
			var myUrl='/api/agenda/contacts';
            var myMethod='POST';
            var myData=$scope.formData;
		} else { 
			var myUrl='/api/agenda/contacts/'+$scope.formData._id;
            var myMethod='PUT';
            var myData=$scope.formData;
		}

		$http({
		    url: myUrl,
            method: myMethod,
            data: myData
		}).success(function(data) {
			console.log(data);
			showToast();
			getContacts();
			go_to('/');
		}).error(function(data) {
			console.log('Error:' + data);
		});
	};

	// Get Contact List
	function getContacts(){
		$http.get('/api/agenda/contacts')
		.success(function(data) {
			$scope.contacts = data;
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		}); 
	};

	// Get Contact
	function getContact(id){
		$http.get('/api/agenda/contacts/'+ id)
		.success(function(data) {
			$scope.formData = data;
			StringToDate();
			console.log(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		}); 
	};

	// Delete Contact
	function deleteContact(id) {
		$http.delete('/api/agenda/contacts/' + id)
		.success(function(data) {
			cleanForm();
			getContacts();
		})
		.error(function(data) {
			console.log('Error:' + data);
		});
	};


	  // ********************************* //
  	 // 		Internal methods		  //
  	// ********************************* //

  	function cleanForm(){
  		$scope.formData = {};
  	}

  	function StringToDate(){
  		$scope.formData.birthday = new Date($scope.formData.birthday);
  	}

  	// Hide or Show the 'left' sideNav area
	function toggleContactList() {
    	$mdSidenav('left').toggle();
  	}

  	function toggleMoreInfo() {
    	if ($scope.expand === "more") $scope.expand = "less";
  		else $scope.expand = "more";
	}

  	function select(url,id) {
  		getContact(id);
  		$location.path(url);
  	}

  	function go_to(url) {
  		cleanForm();
    	$location.path(url);
    }

    function showToast() {
	    $mdToast.show(
	      $mdToast.simple()
	        .textContent('contact saved successfull!')
	        .theme('success-toast')
	        .hideDelay(3000)
	    );
  	};

    $scope.showConfirm = function(id) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Are you sure you want delete?')
          .ariaLabel('Confirm')
          .ok('Yes')
          .cancel('Cancel');

    $mdDialog.show(confirm).then(function(){deleteContact(id);});
  	};

}]);