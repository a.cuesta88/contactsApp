# Contact List MEAN-stack

This branch contains a full-stack personal project with MEAN stack and Angular Material technologies.
A contact list web app completely designed and created by me.

![alt tag](/img/snaphot.png)

Above is a snaphot of my Contact List App with a **Master-Detail** layout: showing a list of contacts (left) and a contact detail view (right).

Also shown is the user experience that will be displayed for smaller device sizes. The responsive layout reveals the **menu** button that can be used to hide the contact list. And the **add** button will be used to show a form for add a new contact.

This MEAN-stack app demonstrates my knowledge about:
	
	1.	NodeJS and ExpressJS (On the server side)
	2.	AngularJS and Angular-Material (On the client side)
	3.	Storing JSON objects in MongoDB
	4.	Use JSON objects to easily transfer data from the database to the server and from the server to the client

Also demonstrates my knowledge about:
	
	1.	To make use of npm, the package manager for JavaScript
	2.	Use of Git


MEAN stack is a technologie used by big companies as Microsoft, eBay, LinkedIn, Yahoo, WalMart, Uber, Oracle, and many more...

## Project structure
	contactsApp
	 |-- /app
	 |---- /controllers
	 |------ contactsController.js // Backend
	 |------ mainController.js    // Frontend
	 |---- /models
	 |------ contact.js 		// Mongoose model
	 |---- /routes
	 |------ routes.js
	 |-- /css
	 |---- app.css
	 |-- /img
	 |---- /icons
	 |-- /partials
	 |---- addContactForm.htm
	 |---- contactInfo.html
	 |-- README.md
	 |-- favicon.ico
	 |-- index.html
	 |-- package.json 
	 |__ server.js // Express server

## Installation

First you must have already installed MongoDB and NodeJS.

Then you need clone the repository and run the following commands on your project folder:

	*	npm update
	*	node server.js